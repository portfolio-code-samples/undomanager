# README #

### What is this repository for? ###

* Implementation of a simple Undo Manager for a text editor
* Version: UndoManager-SNAPSHOT-0.0.1

### How do I get set up? ###

* Clone the git repository: `git clone https://portfolio-code-samples@bitbucket.org/portfolio-code-samples/undomanager.git`
* Navigate to the project root folder: `cd undomanager`
* Run the unit tests using Maven: `mvn test`
