package undo;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import undo.impl.ChangeFactoryImpl;
import undo.impl.DummyTestDocument;
import undo.impl.UndoManagerFactoryImpl;
import undo.impl.UndoManagerImpl;

/**
 * Unit test for {@link UndoManagerImpl}
 */
public class UndoManagerImplTest
{
    private static final int DEF_UM_SIZE = 10;

    private final UndoManagerFactory mUndoManagerFactory = new UndoManagerFactoryImpl();
    private final ChangeFactory mChangeFactory = new ChangeFactoryImpl();

    private Document mDocument;

    @Before
    public void setUp()
    {
        mDocument = new DummyTestDocument();
    }

    private Change insert( int pos, String s, int oldDot, int newDot )
    {
        Change ins = mChangeFactory.createInsertion( pos, s, oldDot, newDot );
        ins.apply( mDocument );
        return ins;
    }

    private Change delete( int pos, String s, int oldDot, int newDot )
    {
        Change del = mChangeFactory.createDeletion( pos, s, oldDot, newDot );
        del.apply( mDocument );
        return del;
    }

    @Test( expected = IllegalStateException.class )
    public void testEmptyUMUndoFails()
    {
        UndoManager um = mUndoManagerFactory.createUndoManager( mDocument, 0 );
        assertFalse( um.canUndo() );
        um.undo();
    }

    @Test( expected = IllegalStateException.class )
    public void testEmptyUMRedoFails()
    {
        UndoManager um = mUndoManagerFactory.createUndoManager( mDocument, 0 );
        assertFalse( um.canRedo() );
        um.redo();
    }

    @Test
    public void testFullUndoBuffer()
    {
        int insertions = DEF_UM_SIZE + 1;
        UndoManager um = mUndoManagerFactory.createUndoManager( mDocument, DEF_UM_SIZE );

        for ( int i = 0; i < insertions; i++ )
        {
            char c = (char) ( 'A' + i );
            Change ins = insert( i, Character.toString( c ), i, i + 1 );
            um.registerChange( ins );
        }

        for ( int i = 0; i < DEF_UM_SIZE; i++ )
        {
            um.undo();
        }

        assertFalse( um.canUndo() );
    }

    @Test
    public void test1ChangeCanUndo() throws IllegalStateException
    {
        UndoManager um = mUndoManagerFactory.createUndoManager( mDocument, DEF_UM_SIZE );

        Change ins = insert( 0, "I0", 0, 0 );
        um.registerChange( ins );
        assertTrue( um.canUndo() );

        um.undo();
        assertFalse( um.canUndo() );
    }

    @Test
    public void test1ChangeUndoCanRedo() throws IllegalStateException
    {
        UndoManager um = mUndoManagerFactory.createUndoManager( mDocument, DEF_UM_SIZE );

        Change ins = insert( 0, "I0", 0, 0 );
        um.registerChange( ins );
        um.undo();
        assertTrue( um.canRedo() );
        um.redo();
    }

    @Test
    public void testCannotRedoAfterChangeRegistered() throws IllegalStateException
    {
        UndoManager um = mUndoManagerFactory.createUndoManager( mDocument, DEF_UM_SIZE );

        Change ins = insert( 0, "I0", 0, 0 );
        um.registerChange( ins );
        um.undo();
        assertTrue( um.canRedo() );

        ins = insert( 0, "I1", 0, 0 );
        um.registerChange( ins );
        assertFalse( um.canRedo() );
    }

    @Test
    public void testUndoRedoLoop()
    {
        UndoManager um = mUndoManagerFactory.createUndoManager( mDocument, DEF_UM_SIZE );

        Change ins = insert( 0, "A", 0, 1 );
        um.registerChange( ins );

        ins = insert( 1, "BC", 1, 3 );
        um.registerChange( ins );

        um.undo();
        um.undo();
        assertThat( "0|", is( mDocument.toString() ) );
        assertFalse( um.canUndo() );

        um.redo();
        um.redo();
        assertThat( "3|ABC", is( mDocument.toString() ) );
        assertFalse( um.canRedo() );
    }

    @Test
    public void testDeleteMiddleChar() throws IllegalStateException
    {
        UndoManager um = mUndoManagerFactory.createUndoManager( mDocument, DEF_UM_SIZE );

        Change ins = insert( 0, "ABCDEF", 0, 6 );
        um.registerChange( ins );

        Change del = delete( 2, "C", 6, 2 );
        um.registerChange( del );
        assertThat( "2|ABDEF", is( mDocument.toString() ) );

        um.undo();
        assertThat( "6|ABCDEF", is( mDocument.toString() ) );

        assertTrue( um.canRedo() );
    }

    @Test
    public void testTheUltimateQuestionOfLife()
    {
        // From "The Hitchhiker's Guide to the Galaxy", by Douglas Adams
        String theAnswer = "The answer to the ultimate question of life, the universe and everything is ";

        UndoManager um = mUndoManagerFactory.createUndoManager( mDocument, DEF_UM_SIZE );

        Change ins = insert( 0, theAnswer, 0, 0 );
        um.registerChange( ins );

        ins = insert( theAnswer.length(), "41", 0, 0 );
        um.registerChange( ins );
        assertThat( "0|" + theAnswer + "41", is( mDocument.toString() ) );

        um.undo();

        ins = insert( theAnswer.length(), "42", 0, 0 );
        um.registerChange( ins );
        assertThat( "0|" + theAnswer + "42", is( mDocument.toString() ) ); // Good

        assertFalse( um.canRedo() ); // The answer can never be 41
    }
}
