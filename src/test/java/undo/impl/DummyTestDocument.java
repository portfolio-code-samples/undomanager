package undo.impl;

import undo.Document;

/**
 * 
 * Dummy text document that keeps data in an internal StringBuilder
 * 
 * Not part of the assignment
 * 
 * @author victor
 *
 */
public class DummyTestDocument implements Document
{
    StringBuilder mTextBuffer = new StringBuilder();
    private int mPosition;

    @Override
    public void delete( int pos, String s )
    {
        // TODO: check for null string? negative position?....
        int end = pos + s.length();

        if ( end > mTextBuffer.length() )
        {
            throw new IllegalStateException( "Incorrect position: " + pos );
        }

        if ( !s.equals( mTextBuffer.substring( pos, end ) ) )
        {
            throw new IllegalStateException( "String not found at pos: " + pos );
        }

        mTextBuffer.delete( pos, end );
        mPosition = pos;
    }

    @Override
    public void insert( int pos, String s )
    {
        // TODO: check for null string? negative position?....
        int end = pos + s.length();
        mTextBuffer.insert( pos, s );
        mPosition += s.length();
    }

    @Override
    public void setDot( int pos )
    {
        mPosition = pos;
    }

    @Override
    public String toString()
    {
        return String.format( "%d|%s", mPosition, mTextBuffer.toString() );
    }
}
