package undo.impl;

import undo.ChangeType;
import undo.Document;

public class InsertChange extends AbstractChange
{
    public InsertChange( int pos, String s, int oldDot, int newDot )
    {
        super( pos, s, oldDot, newDot );
    }

    @Override
    public String getType()
    {
        return ChangeType.INS.name();
    }

    @Override
    public void apply( Document doc )
    {
        doc.insert( mPos, mText );
        doc.setDot( mNewDot );
    }

    @Override
    public void revert( Document doc )
    {
        doc.delete( mPos, mText );
        doc.setDot( mOldDot );
    }
}
