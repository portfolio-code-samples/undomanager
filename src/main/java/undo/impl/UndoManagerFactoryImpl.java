package undo.impl;

import undo.Document;
import undo.UndoManager;
import undo.UndoManagerFactory;

/**
 * A factory for {@link UndoManager}s.<br>
 * 
 * This factory could create different {@link UndoManger}s depending on the
 * nature of the Document for instance<br>
 * 
 * Also, the factory could be injected using and injection framework like
 * Spring, instead of explicitly initialized by the App
 *
 */
public class UndoManagerFactoryImpl implements UndoManagerFactory
{
    @Override
    public UndoManager createUndoManager( Document doc, int bufferSize )
    {
        return new UndoManagerImpl( doc, bufferSize );
    }
}
