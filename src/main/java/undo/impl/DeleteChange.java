package undo.impl;

import undo.ChangeType;
import undo.Document;

public class DeleteChange extends AbstractChange
{
    public DeleteChange( int pos, String s, int oldDot, int newDot )
    {
        super( pos, s, oldDot, newDot );
    }

    @Override
    public String getType()
    {
        return ChangeType.DEL.name();
    }

    @Override
    public void apply( Document doc )
    {
        doc.delete( mPos, mText );
        doc.setDot( mNewDot );
    }

    @Override
    public void revert( Document doc )
    {
        doc.insert( mPos, mText );
        doc.setDot( mOldDot );
    }
}
