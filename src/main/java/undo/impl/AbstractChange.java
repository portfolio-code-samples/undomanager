package undo.impl;

import undo.Change;

/**
 * 
 * Change Implementation for testing
 * 
 * @author victor
 *
 */
public abstract class AbstractChange implements Change
{
    protected int mPos;
    protected final String mText;
    protected final int mOldDot;
    protected final int mNewDot;

    public AbstractChange( int pos, String s, int oldDot, int newDot )
    {
        mPos = pos;
        mText = s;
        mOldDot = oldDot;
        mNewDot = newDot;
    }

    @Override
    public String toString()
    {
        return String.format( "%s -> (%d, \"%s\"), cursor: %d -> %d", getType(), mPos, mText, mOldDot, mNewDot );
    }
}
