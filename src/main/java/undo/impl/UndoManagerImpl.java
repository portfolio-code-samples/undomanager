package undo.impl;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;

import undo.Change;
import undo.Document;
import undo.UndoManager;

/**
 * 
 * Non-synchronized implementation of an {@link UndoManager}<br>
 * 
 * Navigating throught the {@link Change}s (Undo/Redo) is implemented using two
 * Stacks<br>
 * 
 * When a change is undone, it is removed from the Undo Stack and put into the
 * Redo stack. And viceversa<br>
 * 
 * When a new {@link Change} is registered, the Redo Stack is cleared (like
 * Eclipse Undo Manager), since it is not possible to apply the existing changes
 * in the new document<br>
 * 
 * The chosen implementation of the Stacks is {@link LinkedList}.
 * {@link LinkedList} is optimized to insert and remove elements at both ends of
 * the queue in O(1).<br>
 * 
 * An alternative could be to implement the stacks as a circular buffer using a
 * fixed size array. That way the memory overhead of the {@link LinkedList}
 * references is eliminated. Or using {@link ArrayDeque}<br>
 *
 * 
 * @author victor
 *
 */
public class UndoManagerImpl implements UndoManager
{
    private final Document mDocument;

    private final int mBufferSize;

    private final Deque<Change> mUndoStack = new LinkedList<>();

    private final Deque<Change> mRedoStack = new LinkedList<>();

    public UndoManagerImpl( Document doc, int bufferSize )
    {
        mDocument = doc;
        mBufferSize = bufferSize;
    }

    @Override
    public void registerChange( Change change )
    {
        mRedoStack.clear();

        if ( mUndoStack.size() >= mBufferSize )
        {
            mUndoStack.removeLast(); // Buffer full, last element lost
        }

        mUndoStack.addFirst( change );
    }

    @Override
    public boolean canUndo()
    {
        return mUndoStack.size() > 0;
    }

    @Override
    public boolean canRedo()
    {
        return mRedoStack.size() > 0;
    }

    @Override
    public void undo()
    {
        if ( !canUndo() )
        {
            throw new IllegalStateException( "Undo buffer empty" );
        }

        Change change = mUndoStack.removeFirst();
        change.revert( mDocument );
        mRedoStack.addFirst( change );
    }

    @Override
    public void redo()
    {
        if ( !canRedo() )
        {
            throw new IllegalStateException( "Redo buffer empty" );
        }

        Change change = mRedoStack.removeFirst();
        change.apply( mDocument );
        mUndoStack.addFirst( change );
    }
}
