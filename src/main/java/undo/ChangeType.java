package undo;

/**
 * 
 * Enum representing the different change types
 * 
 * @author victor
 *
 */
public enum ChangeType
{
    INS, DEL;
}
